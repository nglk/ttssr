# ttssr 
This project is part of the OuNuPo project of the Experimental Publishing course in Master of Media Design in Piet Zwart Institute. 
<https://issue.xpub.nl/05/><br/>
<https://git.xpub.nl/OuNuPo-make/><br/>
<https://xpub.nl/><br/>

This is a different and separated version of my own proposal within the project.
(Loop: text to speech-speech recognition)

## Author
Angeliki Diakrousi

## Description
My collection of texts 'From Tedious Tasks to Liberating Orality: Practices of the Excluded on Sharing Knowledge', refers to orality in relation to programming, as a way of sharing knowledge including our individually embodied position and voice. The emphasis on the role of personal positioning is often supported by feminist theorists. Similarly, and in contrast to scanning, reading out loud is a way of distributing knowledge in a shared space with other people, and this is the core principle behind the ttssr-> Reading and speech recognition in loop software. Using speech recognition software and python scripts I propose to the audience to participate in a system that highlights how each voice bears the personal story of an individual. In this case the involvement of a machine provides another layer of reflection of the reading process. 

### output.txt
According to Kittler (1999, pg. 221) “A desexualized writing profession, distant from any authorship, only empowers the domain of text processing. That is why so many novels written by recent women writers are endless feedback loops making secretaries into writers”. 
My choice for input to this software is an extract of the text 'Many Many Women' of Gertrude Stein. The input can be any text of your choice. 

### References
Kittler, F.A., (1999) Typewriter, in: Winthrop-Young, G., Wutz, M. (Trans.), Gramophone, Film, Typewriter. Stanford University Press, Stanford, Calif, pp. 214–221 <br/>
Stein, G., 2005. Many Many Women, in: Matisse Picasso and Gertrude Stein With Two Shorter Stories

## Installation
### Requirements:
* Python3
* GNU make

### Install Dependencies: 
* pip3: `sudo apt install python3-pip`
* PocketSphinx package: `sudo aptitude install pocketsphinx pocketsphinx-en-us`
* PocketSphinx Python library: `sudo pip3 install PocketSphinx`
* Other software packages:`sudo apt-get install gcc automake autoconf libtool bison swig python-dev libpulse-dev`
* Speech Recognition Python library: `sudo pip3 install SpeechRecognition`
* TermColor Python library: `sudo pip3 install termcolor`
* PyAudio Python library: `sudo pip3 install pyaudio`

### Clone Repository
`git clone https://gitlab.com/nglk/ttssr.git`

### Make command
Sitting inside a pocket(sphinx)
Speech recognition feedback loops using the first sentence of a scanned text as input

run: <br/>
`cd ttssr` <br/>
`bash src/ttssr-loop-human-only.sh ocr/output.txt`


## Licenses:
© 2018 WTFPL – Do What the Fuck You Want to Public License. <br/>
© 2018 BSD 3-Clause – Berkeley Software Distribution 





